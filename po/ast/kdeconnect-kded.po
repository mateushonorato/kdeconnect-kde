# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# SPDX-FileCopyrightText: 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-19 00:38+0000\n"
"PO-Revision-Date: 2024-03-27 23:43+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: kdeconnectd.cpp:56
#, kde-format
msgid ""
"Pairing request from %1\n"
"Key: %2"
msgstr ""

#: kdeconnectd.cpp:64
#, kde-format
msgid "Open"
msgstr ""

#: kdeconnectd.cpp:67
#, kde-format
msgid "Accept"
msgstr ""

#: kdeconnectd.cpp:70
#, kde-format
msgid "Reject"
msgstr ""

#: kdeconnectd.cpp:73
#, kde-format
msgid "View key"
msgstr ""

#: kdeconnectd.cpp:143 kdeconnectd.cpp:145
#, kde-format
msgid "KDE Connect Daemon"
msgstr ""

#: kdeconnectd.cpp:151
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: kdeconnectd.cpp:155
#, kde-format
msgid ""
"Launch a private D-Bus daemon with kdeconnectd (macOS test-purpose only)"
msgstr ""
